import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const DEBUG = !process.argv.includes('--release');
const VERBOSE = process.argv.includes('--verbose');

const config = {
    context: path.resolve(__dirname, '../src'),

    entry: [
        './index'
    ],

    output: {
        path: path.resolve(__dirname, "../www/"),
        filename: '[name].js',
        publicPath: ''
    },
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loaders: ['eslint'],
                include: [
                    path.resolve(__dirname, "../src")
                ]
            }
        ],
        loaders: [
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: /node_modules/,
                loader: 'url-loader',
                query: {
                    regExp: '\/node_modules\/(.*)',
                    name: DEBUG ? '[1]?[hash]' : '[hash].[ext]',
                    limit: 10000
                }
            },
            {
                test: /\.(eot|ttf|wav|mp3)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: /node_modules/,
                loader: 'file-loader',
                query: {
                    regExp: '\/node_modules\/(.*)',
                    name: DEBUG ? '[1]?[hash]' : '[hash].[ext]'
                }
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: /node_modules/,
                loader: 'url-loader',
                query: {
                    name: DEBUG ? '[path][name].[ext]?[hash]' : '[hash].[ext]',
                    limit: 10000
                }
            },
            {
                test: /\.(eot|ttf|wav|mp3)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                query: {
                    name: DEBUG ? '[path][name].[ext]?[hash]' : '[hash].[ext]'
                 }
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.css$/,
                //include: /node_modules/,
                loader: ExtractTextPlugin.extract(`css-loader?${JSON.stringify({
                    sourceMap: DEBUG,
                    importLoaders: 1,
                    localIdentName: DEBUG ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',
                    minimize: !DEBUG
                })}!postcss-loader?sourceMap`)
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                loaders: [
                    'style-loader',
                    `css-loader?${JSON.stringify({
                        sourceMap: DEBUG,
                        localIdentName: DEBUG ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',
                        minimize: !DEBUG
                    })}`,
                    'postcss-loader?sourceMap',
                    `sass-loader?${JSON.stringify({
                        sourceMap: DEBUG
                    })}`
                ]
            },
            {
                test: /\.styl$/,
                loader: ExtractTextPlugin.extract(`css-loader?${JSON.stringify({
                    sourceMap: true,
                    importLoaders: 2,
                    localIdentName: DEBUG ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',
                    minimize: !DEBUG
                })}!postcss-loader?sourceMap!stylus-loader?paths=node_modules&sourceMap&resolve url`)
            },
            {
                test: /\.js$|\.jsx$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: [
                    path.resolve(__dirname, '../src')
                ],
                query: {
                    cacheDirectory: DEBUG,
                    babelrc: false,
                    presets: [
                        'react',
                        'es2015',
                        'stage-0'
                    ],
                    plugins: [
                        'transform-runtime',
                        ...DEBUG ? [] : [
                            'transform-react-remove-prop-types',
                            'transform-react-constant-elements',
                            'transform-react-inline-elements'
                        ]
                    ]
                }
            }
        ]
    },
    resolve: {
        root: path.resolve(__dirname, '../src'),
        modulesDirectories: ['node_modules'],
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json', '.styl', '.scss', '.css']
    },

    cache: DEBUG,
    debug: DEBUG,

    stats: {
        colors: true,
        reasons: DEBUG,
        hash: VERBOSE,
        version: VERBOSE,
        timings: true,
        chunks: VERBOSE,
        chunkModules: VERBOSE,
        cached: VERBOSE,
        cachedAssets: VERBOSE
    },

    postcss: [ require('autoprefixer')({ browsers: ['last 2 versions'] }) ],

    plugins: [
        new ExtractTextPlugin('[name].css', { allChunks: true }),
        new webpack.DefinePlugin({'process.env.NODE_ENV': DEBUG ? '"development"' : '"production"'}),
        new webpack.NoErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        ...DEBUG ? [] : [
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    screw_ie8: true, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    warnings: VERBOSE
                }
            }),
            new webpack.optimize.AggressiveMergingPlugin()
        ]
    ],
    devtool: DEBUG ? 'source-map' : false
};

export default config;
