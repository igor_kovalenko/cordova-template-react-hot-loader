import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import webpackConfig from './webpack.config.js';
import HtmlWebpackPlugin from 'html-webpack-plugin';

webpackConfig.entry.unshift("webpack-dev-server/client?http://localhost:3000/", "webpack/hot/dev-server");
webpackConfig.plugins.unshift(new HtmlWebpackPlugin({template: 'index.html'}));


new WebpackDevServer(webpack(webpackConfig), {
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    historyApiFallback: true,
    stats: webpackConfig.stats
}).listen(3000, 'localhost', function (err) {
    if (err) {
        console.log(err);
    }

    console.log('Listening at localhost:3000');
});
