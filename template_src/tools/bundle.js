import Webpack from 'webpack';
import webpackConfig from './webpack.config';
import process from 'process';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import del from 'del';

const CORDOVA = process.argv.includes('--cordova');

webpackConfig.plugins.unshift(
    CORDOVA ?
        new HtmlWebpackPlugin({template: 'index.cordova.html'}) :
        new HtmlWebpackPlugin({template: 'index.html', inject: true})
);

del.sync(['www/*'], { dot: true });

new Webpack(webpackConfig).run((err, stats) => {
    if (err) {
        console.error(err.stack);
        process.exit(1);
    }
    console.log(stats.toString(webpackConfig.stats));
});
