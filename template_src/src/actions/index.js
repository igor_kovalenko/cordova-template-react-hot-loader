import { OPEN_SIDEMENU, CLOSE_SIDEMENU } from '../constants';

export const openSideMenu = () => ({
    type: OPEN_SIDEMENU
});

export const closeSideMenu = () => ({
    type: CLOSE_SIDEMENU
});

