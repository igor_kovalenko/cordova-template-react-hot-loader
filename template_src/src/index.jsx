import React from 'react';
import { render } from 'react-dom';
import ons from 'onsenui';

/***** components */
import { App } from './containers';

/***** styles */
import 'onsenui/css/onsenui.css';

/***** redux */
import {Provider} from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import * as reducers from 'reducers';


const logger = createLogger();
const store = createStore(combineReducers(reducers),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
    process.env.NODE_ENV === 'production'
        ? applyMiddleware(thunk)
        : applyMiddleware(thunk, logger)
);

ons.ready(() => render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
));
