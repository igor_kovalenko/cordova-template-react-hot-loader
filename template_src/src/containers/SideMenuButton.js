import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    ToolbarButton,
    Icon,
} from 'react-onsenui';

import * as Actions from '../actions';

const SideMenuButton = ({actions}) => (
    <ToolbarButton onClick={actions.openSideMenu}>
        <Icon icon='ion-navicon, material:md-menu'/>
    </ToolbarButton>
);

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(undefined, mapDispatchToProps)(SideMenuButton);
