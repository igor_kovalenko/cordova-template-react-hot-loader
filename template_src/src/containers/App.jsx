import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ons from 'onsenui';
import { Navigator, Splitter, SplitterSide, SplitterContent } from 'react-onsenui';
import * as Actions from '../actions';

/***** components */
import MainPage from '../components/MainPage';
import SideMenu from './SideMenu';

/***** custom styles */
import '../stylus/index.styl';


const App = React.createClass({
    renderPage(route, navigator){
        return(
            <route.component key={route.key} navigator={navigator}/>
        )
    },
    componentDidMount(){
        //const self = this;
        //ons.orientation.on('change', function(){
        //    (ons.orientation.isPortrait() ? self.props.actions.openSideMenu : self.props.actions.closeSideMenu)();
        //});
    },
    componentWillMount(){
    },

    render() {
        return (
            <Splitter>
                <SplitterSide
                    style={{boxShadow: '0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)'}}
                    side='left'
                    width={200}
                    collapse={true}
                    //collapse={ons.orientation.isPortrait()}
                    isSwipeable={true}
                    isOpen={this.props.isOpenSideMenu}
                    onClose={this.props.actions.closeSideMenu}
                >
                    <SideMenu/>
                </SplitterSide>
                <SplitterContent>
                    <Navigator
                        renderPage={this.renderPage}
                        initialRoute={{component: MainPage, key: 'MAIN_PAGE'}}
                    />
                </SplitterContent>
            </Splitter>
        );
    }
});

const mapStateToProps = (state) => ({
    isOpenSideMenu: state.sideMenu.open
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
