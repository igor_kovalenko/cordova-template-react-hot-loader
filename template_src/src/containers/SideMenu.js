import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    Page,
    List,
    ListItem
} from 'react-onsenui';

import * as Actions from '../actions';

import * as stylus from '../stylus/index.styl';

const SideMenu = ({actions}) => {
    return (
        <Page>
            <section style={{margin: '16px'}}>
                <p>
                    Main menu
                </p>
            </section>
            <List
                dataSource={[
                    {title: 'MENU ITEM #1', key: 'menu_item_1', action: () => {}}
                ]}
                renderRow={(row) => (
                        <ListItem key={row.key} tappable
                            style={{color: stylus['highlight-color']}}
                            onClick={() => {row.action(); actions.closeSideMenu();}}>{row.title}</ListItem>
                    )}
            />
        </Page>
    );
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(undefined, mapDispatchToProps)(SideMenu);
