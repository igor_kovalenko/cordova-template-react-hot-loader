import React from 'react';

import {
    Toolbar,
    BackButton
} from 'react-onsenui';

import SideMenuButton from '../containers/SideMenuButton';

const NavApp = ({title, navigator, backButton, sideMenuButton}) => (
    <Toolbar>
        <div className='left'>
            {sideMenuButton ? <SideMenuButton/> : null}
            {backButton ? <BackButton onClick={() => { navigator.popPage(); }}>Back</BackButton> : null}
        </div>
        <div className='center'>{title}</div>
    </Toolbar>
);

export default NavApp;
