import React from 'react';
import { Page, Button } from 'react-onsenui';
import NavBar from './NavBar';

import SecondPage from './SecondPage';

const MainPage = ({navigator}) => (
  <Page renderToolbar={() => <NavBar title='Main Page' sideMenuButton={true} navigator={navigator} />}>
      <Button
          style={{margin: '16px'}}
          modifier='outline'
          onClick={() => {navigator.pushPage({component: SecondPage, key: 'SECOND_PAGE'});}}
      >Second Page</Button>
  </Page>
);

export default MainPage;
