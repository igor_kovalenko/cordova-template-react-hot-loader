import React from 'react';
import { Page } from 'react-onsenui';
import NavBar from './NavBar';

const MainPage = ({navigator}) => (
  <Page renderToolbar={() => <NavBar title='Second Page' backButton={true} navigator={navigator} />}/>
);

export default MainPage;
