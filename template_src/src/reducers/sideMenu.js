import { OPEN_SIDEMENU, CLOSE_SIDEMENU } from '../constants';

const sideMenu = (state = {open: false}, action) => {
    switch (action.type) {
        case OPEN_SIDEMENU:
            return { open: true };
        case CLOSE_SIDEMENU:
            return { open: false };
        default:
            return state;
    }
};
export default sideMenu;
